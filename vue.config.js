const proxy = 'https://webhook.site/77a13bff-1144-49c2-9ee1-24a46d28f8d9'
module.exports = {
  publicPath: '/app',
  devServer: {
    port: 80,
    disableHostCheck: true,
    proxy: {
      '/api': {
        target: typeof proxy !== 'undefined' ? proxy : 'http://127.0.0.1:8080',
        changeOrigin: true
      },
    },
  },

  transpileDependencies: ['vuetify'],

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false,
    },
  },
}
